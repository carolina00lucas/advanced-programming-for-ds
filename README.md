# Group_F

Group F is composed by 6 motivaded data scientists looking to make the world a liitle bit greener.

Get to know us :)

Ana Sofia Simões : 45920@novasbe.pt 
Carolina Lucas : 44364@novasbe.pt 
Emila Aguiar : 44993@novasbe.pt 
Maria Madalena Neves : 26159@novasbe.pt
Mariana Cunha : 44789@novasbe.pt 
Patricia Macedo : 44359@novasbe.pt 

## Configurations
run the following code on the terminal to activate the environment :
conda env create -f groupenv.yml

If user is in a mac, the above should work. However, some bugs in the recent anaconda version might impede it to work on other computers. To be noted that is not of the group's responsibility.

User might algo try to use
conda env create -f groupf.yml
However, it suffers the same issue.

The user can still check the content of each yml file to see the packages needed to run the present work.

## Run the code
After activating the groupf environment, open the "Showcase Notebook - Group F" and execute all cells.
