"""
Downloads a zip file to the downloads folder and reads the csv inside with the hourly bike rentals.
Plots a correlation matrix of month, humidity, weather situation, temperature, windspeed, the total number of bike rentals;
the number of bike rentals per hour in a given week;
the average number of rentals for each month, considering the years of 2011 and 2012;
and the expected weekly average rental for bikes in a given month with a 168-hour window.
"""

import os
import zipfile
from urllib.request import urlretrieve
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
import datetime
from typing import Union
import matplotlib.dates as mdates




class ErrorCreateColumn(Exception):
    """Couldn't create column """
    pass

class ErrorPlotGraph(Exception):
    pass


class BikesAnalysis:  
    """
    A class used to analyse the of the number of bike rentals.

    Attributes
    ----------
    file_link : str
        a formatted string that contains the link to download the zipfile with the dataset
    output_file : str
        the name for the zipfile to be saved as in the downloads directory

    Methods
    -------
    download_file()
        Downloads a file from the URL defined by file_link into you computer.
    read_csv()
        Reads a csv file from a zip file.
    corr_matrix()
        Plots correlation matrix 
    plot_week()
        Plots the number of total rental bikes in a given week
    plot_month()
        Plots the average number of bike rentals for each month of the year
    forecast()
        Plots the expected weekly average rental for bikes in a given month
        
        
    """

    def __init__(self, file_link : str, output_file : str):   
        """
        Parameters
        ----------
        file_link : str
            a formatted string that contains the link to download the zipfile with the dataset
        output_file : str
            the name for the zipfile to be saved as in the downloads directory
        """   
        self.file_link = file_link
        self.output_file = output_file
        self.path=None
        self.bikes_frame=None

    def download_file(self):
        """
        Downloads a file from an URL into your computer to the downloads folder in the present directory.
        If the downloads folder does not exist, one will be created.
        If the file already exists in the folder, it will not downloaded again.

        Parameters
        ------------
        file_link: str
            A string data type containing the link to the file to be downloaded.
        output_file: str
        A string data type revealing the name of the output file. If NONE the default value is 'file.csv', 
            returning a file of type .csv at the location you are running the function.
        """
        if 'downloads' not in os.listdir():
            os.mkdir('downloads')
            
        self.path = 'downloads/'+str(self.output_file)

        if not os.path.exists(self.path):
            urlretrieve(self.file_link, filename=self.path)
        else:
            print("The file exists in your hard drive.")

    def read_csv(self):

        """
        Reads a csv file from a zip file.

        Returns
        ---------
        The csv file as a pandas Dataframe. This will be a class attribute by the name of bikes_frame.

        Example
        ---------
        read_csv(zip_file)
        """
        try:
            zip_contents = zipfile.ZipFile(self.path)
            self.bikes_frame = pd.read_csv(zip_contents.open('hour.csv'))
            
            df = self.bikes_frame
            df['dteday'] = pd.to_datetime(df['dteday'])
            df['hr_d'] = pd.to_timedelta(df['hr'],'hours')
            df['Datetime'] = df['dteday'] + df['hr_d']


            #new index
            df.set_index('Datetime',drop = False, inplace = True)
        except:
            print("The file you are trying to read doesn't exist.")

        display(df)


    def corr_matrix(self):
        """
        Plots correlation matrix

        Returns
        ------------
        Returns dataframe with a correlation matrix between the number of rented bikes
        and the following variables: month, humidity, weather, temperature, and windspeed.
        """
        df = self.bikes_frame

        if not isinstance(df,pd.DataFrame):
            raise TypeError("Variable 'dataframe' is not a pandas dataframe.")
        else:
            corr = self.bikes_frame.corr()[['mnth', 'hum', 'weathersit', 'temp', 'windspeed','cnt']].loc[['mnth', 'hum', 'weathersit', 'temp', 'windspeed', 'cnt'], :]
            display(corr)
        
            sns.heatmap(corr, annot=True, cmap='RdYlBu', annot_kws={'size':14}, fmt=".2f")
            plt.title("Correlation Matrix")
            plt.show()


    def plot_week(self ):
        """
        Plots the number of total rental bikes in a given week

        Returns
        ------------
        Plots the number of total rental bikes in a given week 
        """
        #Create a new column called week
        try:
            dataframe =  self.bikes_frame
            dataframe['week'] = (dataframe.index.isocalendar().week + (dataframe.index.year - dataframe.index.year.min()) * 52)
        except Exception:
            raise ErrorCreateColumn("Couldn't create a week column ")

        #Plot the graph
        try:
            week = int(input("Insert the week"))
            if type(week) != int:
                raise TypeError("Variable 'week' is not an integer")
            if week not in range(103):
                raise TypeError("Variable 'week' is not in the range of 0 and 102 weeks")
                
                
            dataframe['week']= np.where((dataframe.index.year==2011) & (dataframe.index.month==1) & (dataframe['week']==52), 1, dataframe['week'])
            dataframe['week'] = np.where((dataframe.index.year ==2012) & (dataframe.index.month ==12) & (dataframe['week']==53), 105, dataframe["week"])
            

            dfweek = pd.DataFrame(dataframe[dataframe['week']==week])
            plt.plot(dfweek["instant"], dfweek["cnt"])
            plt.xlabel("Record index")
            plt.ylabel("Count of total rental bikes")
            plt.title("Total Rental Bikes per Instant in Week %d" %week)
            
        except Exception:
            raise ErrorPlotGraph("Couldn't plot the graph")

    
    def plot_month(self):
        """
        Plots the average number of bike rentals for each month of the year, using a bar plot

        Returns
        ------------
        Bar plot with the average number of bike rentals for each month of the year.
        The name of the month is the x-axis and the number of rentals is the y-axis.
        """

        if not np.issubdtype(self.bikes_frame.Datetime.dtype, np.datetime64):
            #change
            raise TypeError("self.bikes_frame.Datetime is not using datetime format")
            
        self.bikes_frame['mnth_new'] = self.bikes_frame['Datetime'].apply(lambda x: x.strftime(format="%B"))
        
        m_y_sum = self.bikes_frame.groupby(['mnth','mnth_new','yr'])[['cnt']].sum()
        avgcount_month = m_y_sum.reset_index().groupby(['mnth','mnth_new'])['cnt'].mean().to_frame()
        avgcount_month = avgcount_month.reset_index()
        
        plt.figure(figsize=(12,7))
        ax = sns.barplot(x=avgcount_month['mnth_new'], y=avgcount_month['cnt'], palette='pastel')
        plt.xlabel("Month")
        plt.ylabel("Total average rentals")
        plt.title("Total average bike rentals per month")

        for p in ax.patches:
                _x = p.get_x() + p.get_width() / 2
                _y = p.get_y() + p.get_height()
                value = int(p.get_height())
                ax.text(_x, _y, value, ha="center")
                
                
    def forecast(self, month: Union[str, int]):  
        """
        Plots the expected weekly average rental for bikes in a given month with a 168-hour window.
        It is given the average rental with a shaded area corresponding to an interval of [-1 std deviation, +1 std deviation.]
        
        Parameters
        -----------        
        month: str or int 
            String or integer argument revealing the name of the month to which the plot will entail 
        
        Returns
        -----------
        Plots the expected weekly rentals for a given month
        """
        # Receive argument as int or str
        if not isinstance( month, (str, int) ):
            raise TypeError("Variable 'month' is neither an integer nor a string")
        else:
            if isinstance( month, str ):
                try:
                    datetime_obj = datetime.datetime.strptime(month,"%B")
                    month = datetime_obj.month
                except:
                    raise TypeError("Variable 'month' is not a month")

            if not month in range(1, 13):
                print("Please type a number that represents a month (between 1 and 12)")
            
            # Aggregating to hour of week and determining the mean and standard deviation 
            else:
                df = self.bikes_frame.copy()
                selected = pd.DataFrame(df[df.index.month == month])
                selected["weekday_new"] = selected.index.weekday
                grouped = selected.groupby(["weekday_new", "hr"])["cnt"].agg(["mean", "std"]).reset_index()

                
            #Plotting
            fig, ax = plt.subplots(figsize=(12,5))
        
            grouped["date_time"] = "1900-01-" + (grouped["weekday_new"]+1).astype(str).str.zfill(2)+"-"+grouped["hr"].astype(str).str.zfill(2)
            grouped["date_time"] = pd.to_datetime(grouped["date_time"], format="%Y-%m-%d-%H")
            self.test = grouped

            ax.fill_between(grouped.date_time,
                            grouped['mean']-grouped['std'],
                            grouped['mean']+grouped['std'],
                            alpha=0.4,label="Standard Deviation")

            ax.plot(grouped.date_time,grouped["mean"], c = "C3",label="Average")

            ax.xaxis.set(
                major_locator = mdates.DayLocator(),
                major_formatter = mdates.DateFormatter("\n\n%A"),
                minor_locator = mdates.HourLocator((0, 12)),
                minor_formatter = mdates.DateFormatter("%H"),
            )

            ax.set_ylabel("Count of rentals")

            month_str = datetime.date(1900, month, 1).strftime('%B')

            plt.title("Expected average weekly rentals in %s" % month_str, loc='left')
            plt.tight_layout()
            plt.legend(loc='upper left')
            plt.show()

            print("Thank you for your time :)" )
